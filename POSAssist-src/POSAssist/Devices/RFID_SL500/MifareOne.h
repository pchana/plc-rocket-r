#ifndef MIFAREONE_H
#define MIFAREONE_H

#if defined(__WIN32__)
#define MIFAREONE_WIN32
#elif defined(__UNIX__)
#define MIFAREONE_UNIX
#endif

extern HINSTANCE m_hInstMaster;
extern unsigned short m_devID;

int sl500_init();
int sl500_close();
int sl500_req_card_ID(char *pBuff, int buff_length);
int sl500_beep_ready();

#endif /* MIFAREONE_H */
