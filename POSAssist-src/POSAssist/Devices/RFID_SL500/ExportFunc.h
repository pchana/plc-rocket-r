#ifndef _EXPORT_FUNC_H_
#define _EXPORT_FUNC_H_

/*
* The original source is from  etc/RFID_SL500_EXAMPLE/VC/ExportFunc.h
*/

//return successful result
#define  LIB_SUCCESS      0  
//return failed result
#define  LIB_FAILED       1

#define MAX_RF_BUFFER     1024

/******** นฆฤฃบป๑ศกถฏฬฌฟโฐๆฑพบล 2ืึฝฺ *******************/
//  ทตปุ: ณษนฆทตปุ0
/*********************************************************/
int (WINAPI* lib_ver)(unsigned int *nVer);


/*****************DES หใทจผำรบฏส ***********************/
//user     : 
//         DES หใทจผำรบฏส
//Parameter: 
//         szOut:สไณ๖ตฤDESึตฃฌณคถศตศำฺร๗ฮฤณคถศ
//         szIn: ร๗ฮฤ
//         inlen:ร๗ฮฤณคถศ,8ืึฝฺตฤี๛สฑถ
//         key:รฮฤ
//         keylen: รฮฤณคถศ,ศ็น๛ด๓ำฺ8ืึฝฺฃฌสว3des,ศ็น๛ะกำฺตศำฺ8ืึฝฺตฅdes.ฒปืใฒนมใ
//return value: 
//         Success: LIB_SUCCESS ;
//         Failed : LIB_FAILED
/*********************************************************/
int (WINAPI* des_encrypt)(unsigned char *szOut,unsigned char *szIn , unsigned int inlen,unsigned char *key,unsigned int keylen);


/******** หตร๗ฃบDES ฝโรหใทจบฏส *************************/
//user     : 
//         DES หใทจฝโรบฏส
//Parameter: 
//         szOut:สไณ๖ตฤDESึตฃฌณคถศตศำฺร๗ฮฤณคถศ
//         szIn: ร๗ฮฤ
//         inlen:ร๗ฮฤณคถศ,8ืึฝฺตฤี๛สฑถ
//         key:รฮฤ
//         keylen: รฮฤณคถศ,ศ็น๛ด๓ำฺ8ืึฝฺฃฌสว3des,ศ็น๛ะกำฺตศำฺ8ืึฝฺตฅdes.ฒปืใฒนมใ
//return value: 
//         Success: LIB_SUCCESS ;
//         Failed : LIB_FAILED
/*********************************************************/
int (WINAPI* des_decrypt)(unsigned char *szOut,unsigned char *szIn , unsigned int inlen,unsigned char *key,unsigned int keylen);


/******** นฆฤฃบณ๕สผปฏดฎฟฺ *******************************/
//  ฒฮสฃบportฃบดฎฟฺบลฃฌศกึตฮช1กซ4
//        baudฃบฮชอจัถฒจฬุยส4800กซ115200
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_init_com)(int port,long baud);


/******** นฆฤฃบึธถจษ่ฑธฑ๊สถ *****************************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛ฃฌ0-65536
//  ทตปุฃบณษนฆทตปุ0
/*********************************************************/
int (WINAPI* rf_init_device_number)(unsigned short icdev);


/******** นฆฤฃบถมศกษ่ฑธฑ๊สถ *****************************/
//  ฒฮสฃบIcdevฃบอจัถษ่ฑธฑ๊สถท๛
//  ทตปุฃบณษนฆทตปุ0
/*********************************************************/
int (WINAPI* rf_get_device_number)(unsigned short *Icdev);


/******** นฆฤฃบศกตรถมะดฟจฦ๗ำฒผฐๆฑพบลฃฌ2 ืึฝฺ ***********/
//  ฒฮสฃบicdevฃบ  อจัถษ่ฑธฑ๊สถท๛
//        Versionฃบดๆทลทตปุฐๆฑพะลฯข
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_get_model)(unsigned short icdev,unsigned short *Version);


/******** นฆฤฃบศกตรถมะดฟจฦ๗ฒ๚ฦทะ๒มะบลฃฌ8 ืึฝฺ ***********/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        Snrฃบ  ดๆทลทตปุถมะดฟจฦ๗ฒ๚ฦทะ๒มะบล
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_get_snr)(unsigned short icdev,unsigned char *Snr);


/******** นฆฤฃบทไร๙ *************************************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        msecฃบ ทไร๙สฑฯฃฌตฅฮปสว10 บมร๋
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_beep)(unsigned short icdev,unsigned char msec);


/******** นฆฤฃบษ่ึรถมะดฟจฦ๗sam ฟจอจัถฒจฬุยส *************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        bound: sam ฟจฒจฬุยสฃฌศกึตฮช9600กข38400
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบbound=0:9600
//        bound=1:38400
/*********************************************************/
int (WINAPI* rf_init_sam)(unsigned short icdev,unsigned char bound);


/******* นฆฤฃบธดฮปsam ฟจ ********************************/
//  ฒฮสฃบicdevฃบ อจัถษ่ฑธฑ๊สถท๛
//        pDataฃบทตปุตฤธดฮปะลฯขฤฺศ
//        pMsgLgฃบทตปุธดฮปะลฯขตฤณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_sam_rst)(unsigned short icdev, unsigned char *pData,unsigned char *pMsgLg);
/*ภฃบ
     unsigned int icdev;
     unsigned char pData[MAX_RF_BUFFER];
     unsigned char len;
     status = rf_sam_rst(icdev,pData,&len);
     
*/


/******** นฆฤฃบฯ๒SAM ฟจทขหอCOS รม๎ *********************/
//  ฒฮสฃบicdevฃบ  อจัถษ่ฑธฑ๊สถท๛
//        commandฃบcos รม๎
//        cmdLen:  cos รม๎ณคถศ
//        pDateฃบ ฟจฦฌทตปุตฤสพฃฌบฌSW1กขSW2
//        pMsgLgฃบ ทตปุสพณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_sam_cos)(unsigned short icdev, unsigned char *command,unsigned char cmdLen ,unsigned char *pData,unsigned char* Length);
/*ภฃบ
     unsigned char icdev;
     unsigned char* cmd;
     unsigned char pData[MAX_RF_BUFFER];
     unsigned char len;
     status = rf_sam_cos(icdev,cmd,sizeof(cmd),pData,&len);
     
*/


/*******  นฆฤฃบษ่ึรถมะดฟจฦ๗ทวฝำดฅนคื๗ทฝสฝฮช *************/ 
//              ISO14443 TYPE A OR ISO14443 TYPE B
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        type:  ถมะดฟจฦ๗นคื๗ทฝสฝ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบtype='A':ษ่ึรฮชTYPE_Aทฝสฝ
//        type='B':ษ่ึรฮชTYPE_Bทฝสฝ
/*********************************************************/
int (WINAPI* rf_init_type)(unsigned short icdev,unsigned char type);


/*******  นฆฤฃบนุฑีป๒ฦ๔ถฏถมะดฟจฦ๗ฬ์ฯ฿ทขษไ ***************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        modelฃบฬ์ฯ฿ืดฬฌ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบmodel=0:นุฑีฬ์ฯ฿
//        model=1:ฟชฦ๔ฬ์ฯ฿
/*********************************************************/
int (WINAPI* rf_antenna_sta)(unsigned short icdev, unsigned char model);


/******** นฆฤฃบัฐISO14443-3 TYPE_A ฟจ *******************/
//  ฒฮสฃบicdevฃบ  อจัถษ่ฑธฑ๊สถท๛
//        modelฃบ  ัฐฟจฤฃสฝ
//        TagTypeฃบทตปุฟจภเะอึต
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบmode=0x26:ัฐฮดฝ๘ศ๋ะร฿ืดฬฌตฤฟจ
//        mode=0x52:ัฐห๙ำะืดฬฌตฤฟจ
/*********************************************************/
int (WINAPI* rf_request)(unsigned short icdev, unsigned char model, unsigned short *TagType);


/********* นฆฤฃบISO14443-3 TYPE_A ฟจทภณๅืฒ **************/
//  ฒฮสฃบicdevฃบ  อจัถษ่ฑธฑ๊สถท๛
//        bcntฃบ   ฟจะ๒มะบลืึฝฺสฃฌศกึต4กข7กข10ฃฌMifare ฟจศกึต4
//        pSnrฃบ  ทตปุตฤฟจะ๒มะบล
//        pRLength:ฟจะ๒มะบลณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_anticoll)(unsigned short icdev, unsigned char bcnt, unsigned char *pSnr,unsigned char* pRLength);
/*ภฃบint status
      unsigned char icdev;
      unsigned char snr[MAX_RF_BUFFER];
      unsigned char len;
      status = rf_anticoll(icdev,4,snr,&len);      
*/


/******** นฆฤฃบห๘ถจาปีลISO14443-3 TYPE_A ฟจ *************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        pSnrฃบ ฟจะ๒มะบล
//        srcLen:ฟจะ๒มะบลณคถศฃฌMifareOneฟจธรึตตศำฺ4
//        Sizeฃบ ทตปุฟจศมฟ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_select)(unsigned short icdev,unsigned char *pSnr,unsigned char srcLen,unsigned char *Size);


/******* นฆฤฃบรม๎าัผคป๎ตฤISO14443-3 TYPE_Aฟจฝ๘ศ๋ะร฿ืดฬฌ*/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//  ทตปุฃบณษนฆิ๒ทตปุ0
/**********************************************************/
int (WINAPI* rf_halt)(unsigned short icdev);

/****** นฆฤฃบฯ๒ถมะดฟจฦ๗ฯยิุMifare One ฟจริฟ ******************/
//รฟ6 ธ๖ืึฝฺฮช1 ธ๖ริฟฃฌ0กซ15 ษศว๘หณะ๒ลลมะ
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        Modeฃบริฟภเะอ,ศกึตกฎAกฏOR กฎBกฏ
//        keyฃบริฟฃฌ96 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/***************************************************************/
int (WINAPI* rf_download_key)(WORD icdev, unsigned char mode, unsigned char *key);

/***** นฆฤฃบำราัฯยิุตฝถมะดฟจฦ๗ึะตฤริฟั้ึคMifare One ฟจฤณาปษศว๘ ****/
//   ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//         modeฃบรย๋ั้ึคฤฃสฝ, ศกึต0 ป๒1ฃฌด๚ฑํกฎAกฏOR กฎBกฏ
//         secnrฃบาชั้ึครย๋ตฤษศว๘บลฃจ0กซ15ฃฉ
//   ทตปุฃบณษนฆิ๒ทตปุ0
/********************************************************************/
int (WINAPI* rf_M1_authentication1)(WORD icdev, unsigned char mode, unsigned char secnr);

/***** นฆฤฃบำรึธถจตฤริฟั้ึคMifare One ฟจ*****************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        modelฃบรย๋ั้ึคฤฃสฝ
//        blockฃบาชั้ึครย๋ตฤพ๘ถิฟ้บลฃจ0กซ63ฃฉ
//        keyฃบ  ริฟฤฺศฃฌ6 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบmodel=0x60:ั้ึคAริฟ
//        model=0x61:ั้ึคBริฟ
/**********************************************************/
int (WINAPI* rf_M1_authentication2)(unsigned short icdev,unsigned char model,unsigned char block,unsigned char *key);


/*******  นฆฤฃบถมศกMifare One ฟจาปฟ้สพ ****************/
//  ฒฮสฃบicdevฃบ อจัถษ่ฑธฑ๊สถท๛
//        blockฃบ M1ฟจพ๘ถิฟ้บลฃจ0กซ63ฃฉ
//        pDataฃบถมณ๖สพ
//        pLen:   ถมณ๖สพตฤณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_M1_read)(unsigned short icdev, unsigned char block, unsigned char *pData,unsigned char *pLen);
/*ภฃบint status
      unsigned short icdev
      unsigned char pData[MAX_RF_BUFFER];
      unsigned char len;
      status = rf_M1_read(icdev,0,pData,&len);
      
*/


/*******  นฆฤฃบฯ๒Mifare One ฟจึะะดศ๋าปฟ้สพ ************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        blockฃบM1ฟจพ๘ถิฟ้บลฃจ0กซ63)
//        dataฃบ ะดศ๋ตฤสพฃฌ16 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_M1_write)(unsigned short icdev, unsigned char block, unsigned char *data);


/*******  นฆฤฃบฝซMifare One ฟจฤณาปษศว๘ณ๕สผปฏฮชวฎฐ *******/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        blockฃบM1 ฟจฟ้ตุึทฃจ0กซ63ฃฉ
//        valueฃบณ๕สผึต
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_M1_initval)(unsigned short icdev, unsigned char block, long value);


/*******  นฆฤฃบถมMifare One วฎฐึต **********************/
//  ฒฮสฃบicdevฃบ อจัถษ่ฑธฑ๊สถท๛
//        blockฃบ M1 ฟจฟ้ตุึทฃจ0กซ63ฃฉ
//        pValueฃบทตปุตฤึต
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_M1_readval)(unsigned short icdev, unsigned char block,long* pValue);


/*******  นฆฤฃบMifare One ฟฟ๎ **************************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        blockฃบM1 ฟจฟ้ตุึทฃจ0กซ63ฃฉ
//        valueฃบาชฟตฤึต
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบดหบฏสึดะะณษนฆบ๓ฃฌฝแน๛ฑฃดๆิฺฟจฦฌตฤBUFFER ฤฺฃฌ
//        ษะฮดธฤะดฯเำฆฟ้ตฤฤฺศฃฌศ๔าชฝซฝแน๛ฑฃดๆตฝฟจฦฌ
//        ฯเำฆฟ้ึะะ่ฝ๔ธ๚ึดะะrf_M1_restore บฏส
/*********************************************************/
int (WINAPI* rf_M1_decrement)(unsigned short icdev, unsigned char block,long value);


/******** นฆฤฃบMifare One ณไึต **************************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        blockฃบM1 ฟจฟ้ตุึทฃจ0กซ63ฃฉ
//        valueฃบาชิ๖ผำตฤึต
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_M1_increment)(unsigned short icdev, unsigned char block,long value);


/******** นฆฤฃบMifare One ฟจึตปุดซ **********************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        blockฃบM1 ฟจฟ้ตุึทฃจ0กซ63ฃฉ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบำรดหบฏสฝซึธถจตฤฟ้ฤฺศดซศ๋ฟจตฤbufferฃฌศปบ๓ฟษำร
//        rf_M1transfer()บฏสฝซbuffer ึะสพิูดซหอตฝมําปฟ้ึะศฅ
/*********************************************************/
int (WINAPI* rf_M1_restore)(unsigned short icdev, unsigned char block);


/****** นฆฤฃบฝซMifare Oneสพดซหอ ***********************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        blockฃบM1 ฟจฟ้ตุึทฃจ0กซ63ฃฉ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบธรบฏสฝ๖ิฺincrementกขdecrementบอrestore รม๎ึฎบ๓ต๗ำรกฃ
/*********************************************************/
int (WINAPI* rf_M1_transfer)(unsigned short icdev, unsigned char block);


/******** นฆฤฃบธดฮปท๛บฯISO14443-A ฑ๊ืผตฤCPU ฟจ **********/
//  ฒฮสฃบicdevฃบ อจัถษ่ฑธฑ๊สถท๛
//        model:  ัฐฟจทฝสฝ
//        pDateฃบทตปุตฤธดฮปะลฯขฤฺศ
//        pMsgLgฃบทตปุธดฮปะลฯขณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_typea_rst)(unsigned short icdev,unsigned char model,unsigned char *pData,unsigned char *pMsgLg);
/*ภฃบint status
      unsigned short icdev
      unsigned char pData[MAX_RF_BUFFER];
      unsigned char len;
      status = rf_typea_rst(icdev,0,pData,&len);      
*/


/******** นฆฤฃบฯ๒ท๛บฯISO14443-4ฑ๊ืผตฤCPUฟจทขหอCOS รม๎***/
//  ฒฮสฃบicdevฃบ  อจัถษ่ฑธฑ๊สถท๛
//        commandฃบcos รม๎
//        cmdLen:  cos รม๎ณคถศ
//        pDateฃบ ฟจฦฌทตปุตฤสพฃฌบฌSW1กขSW2
//        pMsgLgฃบ ทตปุสพณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_cos_command)(unsigned short icdev,unsigned char *command,unsigned char cmdLen,unsigned char *pData,unsigned char* pMsgLg);
/*ภฃบint status
      unsigned short icdev
      unsigned char* cmd;
      unsigned char pData[MAX_RF_BUFFER];
      unsigned char len;
      status = rf_typea_cos(icdev,cmd,sizeof(cmd),pData,&len);      
*/


/******** นฆฤฃบผคป๎ท๛บฯISO14443 TYPE_B ฑ๊ืผตฤฟจ *********/
//  ฒฮสฃบicdevฃบ อจัถษ่ฑธฑ๊สถท๛
//        modelฃบ ัฐฟจทฝสฝ0ฃฝREQB,1=WUPB
//        pDateฃบฟจฦฌทตปุตฤสพ
//        pMsgLgฃบทตปุสพตฤณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_atqb)(unsigned short icdev,unsigned char model,unsigned char *pData,unsigned char *pMsgLg);
/*ภฃบint status
      int icdev
      unsigned char msglg
      unsigned char pDate[MAX_RF_BUFFER];
      status = rf_atqb(icdev,0,pDate,&msglg);      
*/

/*********  นฆฤฃบผคป๎าััฐตฝตฤท๛บฯISO14443-B ฑ๊ืผตฤฟจ ************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        PUPI ฃบ ฟจฦฌฮจาปฑ๊สถท๛
//        CID  ฃบ ึธถจธรฟจฦฌสนำรตฤย฿ผญตุึท,ศกึต0กซ14 วาะกำฺslotmax
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*****************************************************************/
int (WINAPI* rf_attrib)(WORD icdev, unsigned long PUPI, unsigned char CID);

/*********  นฆฤฃบฯ๒ท๛บฯISO14443-B ฑ๊ืผตฤCPU ฟจทขหอCOS รม๎ ******/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        CID  ฃบฟจฦฌย฿ผญตุึท
//        commandฃบcos รม๎
//        MsgLgฃบทตปุสพณคถศ
//        Dateฃบ ฟจฦฌทตปุตฤสพฃฌบฌSW1กขSW2
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*****************************************************************/

int (WINAPI* rf_typeb_cos)(WORD icdev, unsigned char CID,unsigned char *command, unsigned char cmdLen,unsigned char *pData,unsigned char *pMsgLg);
/*
ภฃบint status
int icdev
unsigned char *command;
unsigned char msglg;
unsigned char pData[MAX_RF_BUFFER];
status = rf_typeb_cos(icdev,0,command,sizeof(command),pData,&msglg);
*/
/******** นฆฤฃบรม๎าปักึะตฤTYPE_Bฟจฝ๘ศ๋HALT ืดฬฌ*********/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        PUPIฃบ ฟจฦฌฮจาปฑ๊สถท๛
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_hltb)(unsigned short icdev,unsigned long PUPI);
//??

/******** นฆฤฃบั้ึคAT88RF020 ฟจรย๋ *********************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        keyฃบ  รย๋ฃฌ8 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_at020_check)(unsigned short icdev, unsigned char *key);


//******* นฆฤฃบถมAT88RF020 ฟจาปาณสพ *******************/
//  ฒฮสฃบicdevฃบ อจัถษ่ฑธฑ๊สถท๛
//        pageฃบ  าณตุึทฃฌฃจ0กซ31ฃฉ
//        pDateฃบทตปุตฤสพ
//        pMsgLen:ทตปุสพตฤณคถศ
//  ทตปุฃบณษนฆิ๒ทตปุ0
//  หตร๗ฃบ
/*********************************************************/
int (WINAPI* rf_at020_read)(unsigned short icdev, unsigned char page, unsigned char *pData,unsigned char* pMsgLen); 
/*
ภฃบint status
    int icdev
    unsigned char pData[MAX_RF_BUFFER];
    unsigned char len;
    status = rf_at020_read(icdev,0,pData,&len);    
*/


/******** นฆฤฃบะดAT88RF020 ฟจาปาณสพ *******************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        pageฃบ าณตุึทฃฌฃจ0กซ31ฃฉ
//        Dateฃบ าชะดศ๋ตฤสพฃฌ8 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_at020_write)(unsigned short icdev, unsigned char page, unsigned char *data);
 

/******** นฆฤฃบLOCK AT88RF020ฟจ**************************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        dateฃบ สพฃฌ4 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_at020_lock)(unsigned short icdev,unsigned char *data);


/******** นฆฤฃบAT88RF020ฟจผฦสบฏส **********************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        dateฃบ สพฃฌ6 ืึฝฺ
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_at020_count)(unsigned short icdev,unsigned char *data);


/******** นฆฤฃบรม๎AT88RF020 ฟจฝ๘ศ๋HALT ืดฬฌ ************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//  ทตปุฃบณษนฆิ๒ทตปุ0
/*********************************************************/
int (WINAPI* rf_at020_deselect)(unsigned short icdev);


/******** นฆฤฃบฟุึฦตฦตฤัีษซ *****************************/
//  ฒฮสฃบicdevฃบอจัถษ่ฑธฑ๊สถท๛
//        color: 0 ,ฯจตฦ
//               1 ,บ์ตฦ
//               2 ,ยฬตฦ
//               3 ,ปฦตฦ
//  ทตปุฃบณษนฆทตปุ0
/*********************************************************/
int (WINAPI* rf_light)(unsigned short icdev,unsigned char color);



/******** นฆฤฃบนุฑีComถหฟฺ ******************************/
//  ทตปุฃบณษนฆทตปุ0
/*********************************************************/
int (WINAPI* rf_ClosePort)();


/******** นฆฤฃบทตปุืดฬฌืึ *******************************/
//  ทตปุฃบดํฮ๓ด๚ย๋
/*********************************************************/
int (WINAPI* rf_GetErrorMessage)();


#endif