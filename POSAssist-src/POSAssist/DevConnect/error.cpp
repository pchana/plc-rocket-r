/*
 * error.c
 *
 * error message routines
 *
 * (C)1999 Stefano Busti
 *
 */

#include "../stdafx.h"
#include "error.h"

#if defined(__WIN32__)
#include <fstream>
#include <wchar.h>

#define  MAX_ERR_CH		1024 //256
TCHAR	 buff[MAX_ERR_CH];
TCHAR	 szlog_level[LOG_LEVEL_LENGTH] = LOG_LEVEL_DEFAULT;
#endif

void error(TCHAR *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
#if defined(__UNIX__)
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
#elif defined(__WIN32__)
	vswprintf_s(buff, MAX_ERR_CH, (const wchar_t *)fmt, ap);
#endif
	va_end(ap);

#if defined(__WIN32__)
	OutputDebugString(TEXT("ERROR : "));
	OutputDebugString(buff);
#endif
}

void errend(TCHAR *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
#if defined(__UNIX__)
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
#elif defined(__WIN32__)
	vswprintf_s(buff, MAX_ERR_CH, (const wchar_t *)fmt, ap);
#endif
	va_end(ap);

#if defined(__WIN32__)
	OutputDebugString(TEXT("ERROR END!!: "));
	OutputDebugString(buff);
#endif
	exit(-1);
}

void perror2(const char *fmt, ...)
{
	char buf[BUFSIZ];
	va_list ap;

	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	
	perror(buf);
}

void perrend(TCHAR *fmt, ...)
{
	TCHAR buf[BUFSIZ];
	va_list ap;

	va_start(ap, fmt);
#if defined(__UNIX__)
	vsprintf(buf, fmt, ap);
#elif defined(__WIN32__)
	vswprintf_s((wchar_t *)buf, BUFSIZ, (const wchar_t *)fmt, ap);
#endif
	va_end(ap);

#if defined(__UNIX__)
	perror(buf);
#elif defined(__WIN32__)
	OutputDebugString(TEXT("PERR END HOOK!!: "));
	OutputDebugString(buf);
#endif
	exit(-1);
}


void to_log_file(TCHAR *log_buff)
{
	char chbuf[512];
	std::ofstream log_file("posassist.log", std::ios_base::out | std::ios_base::app );

/** I dropped the locale setting here, since I don't how to code it in VC++. **/
//	std::locale utf8_locale;
//	log_file.imbue(utf8_locale);

/** 
 Here are some more info about the locale setting:
 [1] http://stackoverflow.com/questions/5026555/c-how-to-write-read-ofstream-in-unicode-utf8
 [2] http://social.msdn.microsoft.com/Forums/vstudio/en-US/2ff45989-6213-495a-a509-0278417eb0ab/utf8-output-using-c-locales?forum=vclanguage
 [3] http://stackoverflow.com/questions/4406895/what-stdlocale-names-are-available-on-common-windows-compilers
 [4] http://msdn.microsoft.com/en-us/library/39cwe7zf.aspx
**/

	sprintf_s(chbuf, "%S", log_buff); /** Start to out log by don't about UNICODE **/
//	swprintf_s(chbuf2, 100, TEXT("%s"), log_buff); /** swprintf_s support UNICODE **/

/**  I don't know why the below statement does not work by VC++ **/
//	log_file << chbuf << std::end;

/**  So, we use the below statements instead **/
	log_file << chbuf;
	log_file.end;
}


void dbg_msg(TCHAR *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
#if defined(__UNIX__)
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
#elif defined(__WIN32__)
	vswprintf_s(buff, MAX_ERR_CH, (const wchar_t *)fmt, ap);
#endif
	va_end(ap);

#if defined(__WIN32__)
	OutputDebugString(buff);
	if (lstrcmpi(LOG_LEVEL_SILENT, szlog_level))
		to_log_file(buff);
#endif
}
